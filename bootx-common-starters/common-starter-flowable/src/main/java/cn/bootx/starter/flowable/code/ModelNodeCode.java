package cn.bootx.starter.flowable.code;

/**
 * 模型节点配置
 * @author xxm
 * @date 2022/9/4
 */
public interface ModelNodeCode {

    /* 用户分配类型 */
    String ASSIGN_USER = "user";
}

# 借鉴和部分参考的开源项目

Spring Cloud Gateway整合Swagger2 Demo，全网首例： 
https://gitee.com/wxdfun/sw

JEECG BOOT 低代码开发平台： 
https://github.com/jeecgboot/jeecg-boot

HZERO-基于微服务架构开源免费的企业级PaaS平台：
https://gitee.com/open-hand/hzero

RuoYi-Vue 全新 Pro 版本： 
https://gitee.com/zhijiantianya/ruoyi-vue-pro

Snowy国产密码算法后台权限管理系统： 
https://gitee.com/xiaonuobase/snowy

表单设计器 k-form-design： 
https://gitee.com/kcz66/k-form-design

Vue微信菜单编辑器: 
https://github.com/hopex/vue-menu

flowable antd vue 的工作流设计器:
https://gitee.com/Vincent-Vic/workflow-bpmn-modeler-antdv

flowable 工作流相关思路和实现 乐之终曲:
https://blog.csdn.net/qq_37143673